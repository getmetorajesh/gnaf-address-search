# Scala, ElasticSearch and GNAF
Let there be a search engine for Australian addresses!

# Tools, Tools, Tools:
- Scala
- ElasticSearch
- Akka Streams

## ElasticSearch
Running elastic search in docker container

```
 docker run --rm -p 9200:9200 -p 9300:9300 --name=elastic -e ELASTIC_PASSWORD=secret  -e http.port=9200 -e http.cors.allow-origin="*" -e http.cors.enabled=true -e http.cors.allow-headers=X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization -e http.cors.allow-credentials=true docker.elastic.co/elasticsearch/elasticsearch:6.2.3
```

NOTE ES.1: In Elastic search 6.0.0 and above there can be only one document/mapping type per index. For ex, for company index there can be only customer mapping type. The proposed way forward is to create index per type. something like "company_customer"

NOTE ES.2: Index name can only be a lowerCase().

NOTE AKKA: If you get the below error in worksheet
`com.typesafe.config.ConfigException$Missing: No configuration setting found for key 'akka'`

Untick "Run worksheet in the compiler process" under "Preferences > Languages & Frameworks > Scala > Worksheet".

TODO:
[ ] Interactive Map(Leaflet) and Geocoding
[ ] Advanced Search