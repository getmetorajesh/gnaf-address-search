import sbt._

object Dependencies {
  val elastic4sVersion = "6.2.4"
  val elastic4sVersionStreams = "5.6.5"
  val alpakkaStreamsVersion = "0.18"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val elastic4sVersionCore = "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion
  lazy val elastic4sVersionHttp =  "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion
  lazy val elastic4sStreams = "com.sksamuel.elastic4s" %% "elastic4s-streams" % elastic4sVersionStreams
  lazy val alpakkaFileConn = "com.lightbend.akka" %% "akka-stream-alpakka-file" % alpakkaStreamsVersion
  lazy val alpakkaCSVConn = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaStreamsVersion
  lazy val alpakkaElasticConn = "com.lightbend.akka" %% "akka-stream-alpakka-elasticsearch" % alpakkaStreamsVersion
}
