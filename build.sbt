import Dependencies._

lazy val root = (project in file(".")).
  settings(
      fork in run := true,
      cancelable in Global := true,
      inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.4",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "scala-elasticsearch-gnaf",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      elastic4sVersionCore,
      elastic4sVersionHttp,
      elastic4sStreams,
      alpakkaFileConn,
      alpakkaCSVConn,
      alpakkaElasticConn
    )
  )
