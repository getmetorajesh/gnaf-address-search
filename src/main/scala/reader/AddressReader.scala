package reader

import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.HttpClient
import common._
/**
  */
object AddressReader {

  val client = HttpClient(ElasticsearchClientUri("localhost", 9200))

  def addressDetailSearch(id: String) = client.execute {
    search(Indices.addressDetailIndex).query(s"""{"match_all":"${id}" """)
  }.await
}
