package writer
import java.io.File
import java.nio.file.{FileSystems, Paths}

import akka.NotUsed
import akka.stream.alpakka.file.scaladsl
import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.stream.{ActorMaterializer, Materializer}
import akka.actor.ActorSystem

import scala.concurrent.duration._
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.alpakka.elasticsearch.{IncomingMessage, OutgoingMessage}
import akka.stream.alpakka.elasticsearch.scaladsl._
import org.elasticsearch.client.RestClient
import org.apache.http.HttpHost
import spray.json.JsObject
import spray.json._
import DefaultJsonProtocol._
import common.Indices
import reader.AddressReader

object Parser {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: Materializer = ActorMaterializer()
  implicit val client: RestClient =
    RestClient.builder(new HttpHost("localhost", 9200)).build()

  def main(args: Array[String]): Unit = {
    val folder =
      "/Users/preethi/Downloads/FEB18_GNAF_PipeSeparatedValue_20180219141901/G-NAF/G-NAF FEBRUARY 2018/Standard"
    val files = (new File(folder)).listFiles().filter(!_.isDirectory)

    //buildFormattedAddress()
    findAddress()
    System.in.read()

  }

  def loadIntoES(files: List[File]): Unit = {
    Source(files).runForeach(file => {
      val Pipe: Byte = '|'
      val fileMeta = file
        .getName()
        .replace(".psv", "") //NSW_LOCALITY_ALIAS_psv
        .replace("_psv", "") // NSW_LOCALITY_ALIAS_psv.psv
        .split("_", 2)
      val (state, fileType) = fileMeta match {
        case Array(x, y) => (x, y)
      }

      val idColumn = fileType + "_PID" // ADDRESS_DETAIL_PID
      FileIO
        .fromPath(Paths.get(file.getAbsolutePath()))
        .via(CsvParsing.lineScanner(Pipe))
        .via(CsvToMap.toMap())
        .map(_.mapValues(_.utf8String))
        .map { document =>
          // println(document)
          if(document.getOrElse(idColumn, "").length == 0) {
            println(document)
          }

          IncomingMessage(Some(document.getOrElse(idColumn, "")), document)
        }
        .via(
          // read note ES.1 about index
          ElasticsearchFlow.create(
            fileType.toLowerCase(),
            fileType,
            ElasticsearchSinkSettings(105, docAsUpsert = true)
          )
        )
        .runWith(Sink.seq)
    })

  }

  def buildFormattedAddress(): Unit = {
    val sourceSettings = ElasticsearchSourceSettings(bufferSize = 10)

    def addressDetailSource = ElasticsearchSource.create(
      Indices.addressDetailIndex, "",
      query = """{"match_all": {}}"""
    )

    def addressDefaultGeoCodeSource() = ElasticsearchSource.create(
      "address_default_geocode", "",
      query = """{"match_all": {}}"""
    )
    /*addressDefaultGeoCode.LATITUDE,
    addressDefaultGeoCode.LONGITUDE    

    FLAT_NUMBER +
    NUMBER_FIRST
    LEVEL_NUMBER
    GET_ADDRESS_SITE(ADDRESS_SITE_PID)
    STREET_LOCALITY.STREET_NAME
    STREET_LOCALITY.STREET_TYPE_CODE
    STREET_LOCALITY.STREET_SUFFIX_CODE

    STREET_LOCALITY_PID
    POSTCODE*/
    val f1 = addressDetailSource
      .map { message =>
        println(message)
       // IncomingMessage(Some(message("id")), message)

      }
      .runWith(
      Sink.seq
      )

  }

  def findAddress(): Unit = {
    val res = AddressReader.addressDetailSearch("a")
    res match {
      case Left(failure) => println("We failed " + failure.error)
      case Right(results) => println(results.result.hits)
    }
  }

  case class formattedAddress(
                             formatted_address_pid: String,
                             formatted_address: String,
                             latitude: String,
                             longitude: String
                             )
  /**
      println(document)
      val flat = document.getOrElse("flat", "")
      val numberFirst = document.getOrElse("flat", "")
      val
      val formattedAddress =
      document("formattedAddress" -> )
  */
}
