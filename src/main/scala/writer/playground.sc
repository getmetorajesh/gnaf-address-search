//package writer
import akka.stream.scaladsl.Source
import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.util.ByteString

import scala.concurrent._
import scala.concurrent.duration._
import java.nio.file.Paths

import akka.stream.ActorMaterializer
import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.ElasticDsl.search
import com.sksamuel.elastic4s.http.HttpClient
import common.Indices
import reader.AddressReader
//import .AddressReader
//import reade
import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.HttpClient

val client = HttpClient(ElasticsearchClientUri("localhost", 9200))

def addressDetailSearch(id: String) = client.execute {
//  search(Indices.addressDetailIndex / "").query(id)
  get(id) from Indices.addressDetailIndex
}.await


addressDetailSearch("s")
